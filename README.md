### Basic project

You can use 1 of the approaches at your discretion:
 - GitLab - git clone https://gitlab.com/ievgen.golovachov/test.git
 - Stackblitz - https://stackblitz.com/edit/angular-ivy-e1egrm (use fork button)

### Task

Using the capabilities of JavaScript, TypeScript, Angular 2+, RxJS, NgRx implement several components
_(the layout is not important)_

### Development environment (requirements)

- make sure that you use LTS version of node. (our recommendation **16.16.0**)
- clean up your local `.npmrc` file. ***hint***: you could find it there: `C:\Users\<local_user>`. You have to do this only in case if there are any pathes inside this file.

### Steps

- make clone the project from Gitlab
- make sure the project is running
- explore the project structure
- implement the component FormComponent
- implement the component ViewComponent
- *create UNIT test (at your discretion)
 
### Description

##### Form Component

The "Submit" button is disabled by default.

The "Submit" button becomes available after editing one of the attributes.

The form includes the following attributes with certain rules:

1. firstName
    - the value should not be greater than 10 characters
    - the value should not be "FirstName"
2. email
    - implement request emulation with a 5 sec delay to check the existing value in the database (it can be a stream with a predefined constant **admin@admin.com** for the testing)
3. password
    - the value should have only unique digits (the digits must not be repeated)
5. password and confirmPassword
    - the values should be identical
6. skills
    - contains "add skill" button
    - when you click the "add skill" button, an input box appears
    - then you can typing values (Ex: JavaScript, Angular, React etx)
    - no need to process duplicate values

After successful completion the form values need to pass to the state management.

##### View Component

Values can be retrieved from the state management and displayed in the template.

Attribute skills:
- the values can have duplicates (**unique values must be rendered**) (from step 6)
- the value "JavaScript" should not be displayed (from step 6)

### Delivery

##### GitLab
1. no need to execute 'Commit', 'Push' commands
2. execute command git add .
3. execute command git diff --cached > test.patch
4. send file 'test.patch' to email

##### Stackblitz
1. send link stackblitz to email

