import {ChangeDetectionStrategy, Component} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'app-view',
  templateUrl: './app-view.component.html'
})
export class AppViewComponent {}
